package com.epam.rd.java.basic.task7;

public class BDconstants {

    public final static String FIND_ALL_USERS = "SELECT * FROM users";
    public final static String DELETE_USERS = "DELETE FROM users WHERE id IN (";
    public final static String GET_USER = "SELECT login FROM users WHERE login = ?";
    public final static String GET_TEAM = "SELECT name FROM teams WHERE name = ?";
    public final static String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
    public final static String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    public final static String GET_USERS_TEAM = "SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = ?";
    public static final String INSERT_INTO_USER_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES(";

}
